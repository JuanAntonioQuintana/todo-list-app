import React from 'react';
import {Text, View, Button, StyleSheet, TextInput, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import { SearchBar, ListItem } from 'react-native-elements';

export default class Detail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            todoArray: [],
            title: '',
            body: '',
        }

        this.state.title = this.props.navigation.getParam('title', 'no-title');
        this.state.body = this.props.navigation.getParam('body', 'no-body');

        
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} }  = navigation.state

        return {
            
          title: '',
          headerRight: (
            <Button
          
              onPress={
                () => navigation.navigate("Todo", params.configureTodo())}
              title="Editar"
              
            />
          ),
        }
    };

    componentDidMount(){
        this.props.navigation.setParams({
            configureTodo: this.configureTodo
        });
          
        /*AsyncStorage.getItem('todos').then((todos) => {
            todos = JSON.parse(todos);
            this.setState({"todoArray": todos});
            console.log(this.state.todoArray.length);

        });*/
    }


    configureTodo = () => {
        console.log("enviado");
        this.props.navigation.navigate('Todo', { title: this.state.title, body: this.state.body})
    }

    render() {
        return (
            <View>
                <Text style={styles.title}>{this.props.navigation.getParam('title', 'no-title')}</Text>
                <View style={styles.textArea}>
                    <Text>{this.props.navigation.getParam('body', 'no-body')}</Text>
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 14,
        margin: 20,
    },
    textArea: {
        fontSize: 14,
        margin: 20
    }
})