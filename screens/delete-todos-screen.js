import React from 'react';
import {Text, View, Button, StyleSheet, TextInput, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import { SearchBar, ListItem, CheckBox } from 'react-native-elements';

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            todos: [],
            arrayTodos: [],
            marked: [],
            markedAux: [],
            elementsToDelete: [],
            textField: "",
            textValue: 'Eliminar todo'
        }

        /*AsyncStorage.getItem('todos').then((todos)=> {
            todos = JSON.parse(todos);
           
            this.setState({"arrayTodos": todos})
        });*/
    }


    static navigationOptions = ({ navigation }) => {
        const { params = {} }  = navigation.state

        return {
            title: 'Mi lista',

            /*headerRight: (
                <Button 
                    onPress={ () => {
                           

                        }

                    }

                    title="Cancelar"
                />
            ),*/
        }
       
    };

    press = (index) => {
        this.state.markedAux = this.state.marked;
        this.state.markedAux[index] = !this.state.markedAux[index];
        this.setState({marked: this.state.markedAux});

        if(this.state.markedAux[index] == true) {
            this.state.elementsToDelete.push(this.state.todos[index]);
            if(this.state.elementsToDelete.length > 0) {
                this.state.textValue = "Eliminar";
            }
        } else {
            
            var index = this.state.elementsToDelete.indexOf(this.state.todos[index]);
            this.state.elementsToDelete.splice(index, 1);

        }
        //console.log("TAMAÑO de elementsToDetele" + this.state.elementsToDelete.length);
        if(this.state.elementsToDelete.length == 0) {
            this.state.textValue = "Eliminar todos";
        }
        /*console.log(this.state.todos[index]);
        console.log(this.state.elementsToDelete);*/
        
        //console.log(this.state.marked);
        //console.log("El elemento tocado ha sido" + index);
    }


    search = (text) => {
        const newData = this.state.arrayTodos.filter(item => {
            const itemData = item.title.toLowerCase();
        
             const textData = text.toLowerCase();
              
             return itemData.indexOf(textData) > -1; 
        });

        this.setState({ todos: newData });
    } 


    componentWillMount() {
        this.events = [
            this.props.navigation.addListener('didFocus', this.loadTodos)
        ]
    }

    deleteTodo = () => {
        //markedTodos = this.state.marked;
        /*unmarkedTodos = this.state.arrayTodos.filter((_, index) => {
            return !markedTodos[index];
        });*/
        /*console.log("ESTTTTTTTTTTTOOOOOOOO ES TEXTFIELD " + this.state.textField);
        console.log("¿QUÉ VALOR TIENE TEXTVALUE");
        console.log(this.state.textValue);
        console.log("FINNNNNNNN");*/

        if(this.state.textValue === "Eliminar todo"){
            arrayEmpty = [];
            AsyncStorage.setItem('todos', JSON.stringify(arrayEmpty));
            this.loadTodos();
        } else {
            var filteredTodos = this.state.arrayTodos.filter((element) => {
                return this.state.elementsToDelete.indexOf(element) === -1;
            });
            this.state.textField = "";
            //console.log("ESTOS SON LOS TODOS RESTANTES:" + filteredTodos[0].title);
            AsyncStorage.setItem('todos', JSON.stringify( filteredTodos));
            this.state.marked = this.state.todos.map(_ => false);
            this.loadTodos();
            //console.log(unmarkedTodos);
        }
    }

    loadTodos = () => {
        AsyncStorage.getItem('todos').then((todos)=> {
            if(todos != null){
                console.log("SE EJECUTA");
                console.log(todos);
                todos = JSON.parse(todos);
                this.setState({"arrayTodos": todos});
                this.setState({"todos": todos});
                
                console.log("TODOODOSODO " + todos.length);
                console.log(todos);
            }
            
        }); 
    }

    render() {

        var { navigate } = this.props.navigation;

        return (
            <View style={{flex:1, flexDirection: "column"}}>
                <SearchBar 
                    onChangeText={(text) => {this.search(text); this.setState({"textField": text})}}
                    value={this.state.textField}
                    placeholder="Type Here..."/> 
                <ScrollView>
                    {
                        this.state.todos.map((todo, index) => (
                            <ListItem 
                                key={index}
                                title={
                                    <CheckBox
                                        title = {todo.title}
                                        onPress = {() => this.press(index)}
                                        checked={this.state.marked[index]}
                                    />
                                }
                            />
                        ))
                    }
                </ScrollView>
                <Button 
                
                    onPress={
                        () => {this.deleteTodo()}
                    } 
                    
                title={this.state.textValue}
                />

                
            </View>

        );
    }
}


const styles=StyleSheet.create({
    
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 90,
        backgroundColor: '#E91E63',
        width: 60,
        height: 60,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24,
    },
    settingButton: {
        position: 'absolute',
       
        left: 50,
        bottom: 90,

        width: 60,
        height: 60,
        
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    settingButtonText: {
        fontSize: 12,
    }
})