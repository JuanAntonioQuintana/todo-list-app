import React from 'react';
import {Text, View, Button, StyleSheet, TextInput, ScrollView, TouchableOpacity, AsyncStorage } from 'react-native';
import { SearchBar, ListItem } from 'react-native-elements';

export default class HomeScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            todos: [],
            arrayTodos : []
        }

        /*AsyncStorage.getItem('todos').then((todos)=> {
            todos = JSON.parse(todos);
           
            this.setState({"arrayTodos": todos})
        });*/
    }


    static navigationOptions = ({ navigation }) => {
        const { params = {} }  = navigation.state

        return {
            title: 'Mi lista',

            headerRight: (
                <Button 
                    onPress={
                        () => navigation.navigate("Delete")
                    }

                    title="Editar"
                />
            ),
        }
       
    };

    

    search = (text) => {
        const newData = this.state.arrayTodos.filter(item => {
            const itemData = item.title.toLowerCase();
        
             const textData = text.toLowerCase();
              
             return itemData.indexOf(textData) > -1; 
        });

        this.setState({ todos: newData });

    } 


    componentWillMount() {
        this.events = [
            this.props.navigation.addListener('didFocus', this.loadTodos)
        ]
    }

    loadTodos = () => {
            
            AsyncStorage.getItem('todos').then((todos)=> {
                if(todos != null){
                    console.log("SE EJECUTA");
                    console.log(todos);
                    todos = JSON.parse(todos);
                    this.setState({"arrayTodos": todos});
                    this.setState({"todos": todos});
                    console.log("TODOODOSODO " + todos.length);
                    console.log(todos);
                }
               
            });
        
    }
    

    render() {

        var { navigate } = this.props.navigation;

        return (
            <View style={{flex:1, flexDirection: "column"}}>
                <SearchBar 
                    onChangeText={(text) => {this.search(text)}}
                    placeholder="Type Here..."/> 
                <ScrollView>
                    {
                        this.state.todos.map((todo, index) => (
                            <ListItem 
                                key={index}
                                title={todo.title}
                                onPress={() => (navigate('Detail', {
                                    title: todo.title,
                                    body: todo.body
                                }))}
                            />
                        ))
                    }
                </ScrollView>
                <TouchableOpacity 
                    onPress={
                        () => navigate("Todo")
                    } 
                    
                    style={styles.addButton}>
                    <Text style={styles.addButtonText}>+</Text>
                </TouchableOpacity>
                

                <View style={styles.container}>
                    <Text>{this.state.todos.length} items</Text>
                </View>
                
               
                
            </View>

        );
    }
}


const styles=StyleSheet.create({
    
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 90,
        backgroundColor: '#E91E63',
        width: 60,
        height: 60,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24,
    },
    settingButton: {
        position: 'absolute',
       
        left: 50,
        bottom: 90,

        width: 60,
        height: 60,
        
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    settingButtonText: {
        fontSize: 12,
    }
})