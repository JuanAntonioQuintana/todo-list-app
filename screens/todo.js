import React, { Component } from "react";
import { Button, Text, View, StyleSheet, TextInput, TouchableOpacity, AsyncStorage } from "react-native";


export default class Todo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            todoArray: [],
            title: '',
            body: '',
            titleFromDetail: '',
            bodyFromDetail: ''
        }

    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} }  = navigation.state

        return {
            title: 'New Todo',

            headerRight: (
                <Button 
                    onPress={
                        () => navigation.navigate("Detail", params.addTodo())
                    }

                    title="Ok"
                />
            ),
        }
       
    };

    componentDidMount(){
        this.props.navigation.setParams({
            addTodo: this.addTodo
        });
          
        /*AsyncStorage.getItem('todos').then((todos) => {
            todos = JSON.parse(todos);
            this.setState({"todoArray": todos});
            console.log(this.state.todoArray.length);

        });*/
    }

    addTodo = () => {

        if(this.state.titleFromDetail != '' && this.state.bodyFromDetail != ''){
            /*var prueba = this.state.todoArray.filter((todo) => todo.title === this.state.titleFromDetail);
            console.log("pruebaaaaa" + prueba.title);*/
            this.state.todoArray = this.state.todoArray.filter(todo => todo.title != this.state.titleFromDetail);
            //this.state.todoArray.splice(this.state.todoArray.findIndex(todo => todo.title === this.state.titleFromDetail), 1);
            
            console.log(this.state.todoArray);
        }

        //console.log(this.state.todoArray.length);

        if (this.state.title && this.state.body){
            this.state.todoArray.push({
                "title": this.state.title,
                "body": this.state.body
              }
            );
            //this.setState({ todoArray: this.state.todoArray});
            //let todo = this.state.todoArray[this.state.todoArray.length-1];   JSON.parse in the preview.js works in this way.
            //let todos = this.state.todoArray;
            /*console.log(this.state.title);
            console.log(this.state.body);*/
            console.log("Prueba test test: " + this.state.todoArray);
            AsyncStorage.setItem('todos', JSON.stringify(this.state.todoArray));
            this.props.navigation.navigate('Detail', { title: this.state.title, body: this.state.body})
            
        }
        console.log(this.state.todoArray);
        /*this.state.todoArray.push("hello");
        console.log(this.state.todoArray);
        this.state.todoArray.length;*/
        
    }

    componentWillMount(){
        this.events = [
            this.props.navigation.addListener("didFocus", this.configure)
        ];
    }

    configure = () => {

        AsyncStorage.getItem('todos').then((todos)=> {
            if(todos != null){
                console.log("SE EJECUTA");
                console.log(todos);
                todos = JSON.parse(todos);
                this.setState({"todoArray": todos})
                console.log("TODOODOSODO " + todos.length);
                console.log(todos);
            }
           
        });
        if(this.props.navigation.getParam('title', '') != ''){
            //Al pulsasr atrás, title tiene el valor anterior porque no se pulsó edit por lo tanto no se ha modificado el valor
            this.state.titleFromDetail = this.props.navigation.getParam('title', 'no-title');
            this.state.bodyFromDetail = this.props.navigation.getParam('body', 'no-body');
            console.log("titlelelelnskelnsle" + this.state.titleFromDetail);
            this.setState({title: this.props.navigation.getParam('title', 'no-title')})
            this.setState({body: this.props.navigation.getParam('body', 'no-body')})
            console.log("Entra por aquí");
           
        } 

        //console.log("Estado:" + this.props.navigation.getParam('title', ''));
    }

    render() {

        const { navigate } = this.props.navigation;
        return (
            <View>
                <Text style={styles.title}>Título</Text>
                <TextInput 
                    clearButtonMode="always"
                    style={styles.textInput}
                    onChangeText={(title) => this.setState({title})}
                    value={this.state.title}
                    placeholder='Texto del título'
                    placeholderTextColor='white'
                    underlineColorAndroid='transparent'>
                </TextInput>
                <Text style={styles.title}>Cuerpo</Text>
                <View style={styles.textAreaContainer}>
                <TextInput
                    clearButtonMode="always"
                    style={styles.textArea}
                    onChangeText={(body) => this.setState({body})}
                    value={this.state.body}
                    underlineColorAndroid="transparent"
                    placeholder="Type something"
                    placeholderTextColor="grey"
                    multiline={true}
                />
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    title: {
        fontSize: 16,
        margin: 20,
        fontWeight: 'bold'
    },
    textInput: {
        alignSelf: 'stretch',
        color: "black",
        marginLeft: 20,
    },
    textAreaContainer: {
        borderColor: '#333333',
        borderWidth: 1,
        height: 150,
        padding: 5,
        margin: 15
    },
    textArea: {
        justifyContent: "flex-start"
    }
})