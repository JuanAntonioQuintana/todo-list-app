import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer, createStackNavigator } from 'react-navigation';

import HomeScreen from './screens/home-screen';
import SettingScreen from './screens/setting-screen';
import TodoScreen from './screens/todo';
import DetailScreen from './screens/detail';
import DeleteScreen from './screens/delete-todos-screen'

const Navigation = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Setting: { screen: SettingScreen },
    Todo: { screen: TodoScreen },
    Detail: { screen: DetailScreen },
    Delete: { screen: DeleteScreen }
  },
  {
    initialRouteName: 'Home'
  }
);

const App = createAppContainer(Navigation);

export default App;